const botName = "Jamie";
const userName ="You";
let chatContainer = document.getElementById('chatBox_content');

// Function to scroll the chat container to the bottom
function scrollToBottom() {
    chatContainer.scrollTop = chatContainer.scrollHeight;
}
function getTime(){
    let dateNow = new Date();
    date = dateNow.getDate();
    month = dateNow.getMonth();
    year = dateNow.getFullYear();
    hours = dateNow.getHours();
    minutes = dateNow.getMinutes();
    seconds = dateNow.getSeconds();
    if (minutes<10){
        minutes = "0" + minutes;
    }
    if (seconds<10){
        seconds = "0" + seconds;
    }
    let time = `${date}/${month+1}/${year} ${hours}:${minutes}:${seconds}`;
    return time; 
}

function greetingBotMessage() {
    let greetingMessage = "Hello! nice to meet you."
    document.getElementById("bot_name").innerHTML = '<p class="name--text">' + botName + '</p>';
    document.getElementById("bot_GreetMessage").innerHTML = '<p class="botText"><span>' + greetingMessage + '</span></p>';
    let time = getTime();
    document.getElementById("date_time").innerHTML = '<p class="date--text">' + time + '</p>';

}
greetingBotMessage();

function getBotResponse(userText) {
    let getResponse = botResponse(userText);
    let bot_Name = '<p class="name--text">' + botName + '</p>';
    let botMessage = '<p class="botText"><span>' + getResponse + '</span></p>';
    let time = getTime();
    let botTime = '<p class="date--text">' + time + '</p>';
    $("#chatBox_content").append(bot_Name);
    $("#chatBox_content").append(botMessage);
    $("#chatBox_content").append(botTime);
    scrollToBottom();

}
function getResponse(){
    let userText = $("#textInput").val();
    // if (userText == "") {
    //     userText = "I love Code Palace!";
    // }
    let time = getTime();
   
    // document.getElementById("user_name").innerHTML = '<p class="user_name--text">' + userName + '</p>';
    let user_Name = '<p class="user_message name--text"><span>' + userName + '</span></p>';
    let userHtml = '<p class="user_message userText"><span>' + userText + '</span></p>';
    let userTime = '<p class="user_message date--text"><span>' + time + '</span></p>';
    
    $("#textInput").val("");
    $("#chatBox_content").append(user_Name);
    $("#chatBox_content").append(userHtml);
    $("#chatBox_content").append(userTime);
    scrollToBottom()
    // document.getElementById("chat-bar-bottom").scrollIntoView(true);
    setTimeout(() => {
        getBotResponse(userText);
    }, 1000)
}


function clickToSend(){
    getResponse();
}

$("#textInput").keypress(function (e) {
    if (e.which == 13) {
        e.preventDefault();
        getResponse();
    }
});