// function botResponse(userInput){
//     if (userInput.includes("!")) {
//         return "Please remain calm";
//     }else if (userInput.includes("?")) {
//         return "Yes";
//     }
// }

function botResponse(userInput){
    userInput = userInput.toLowerCase();
    switch (true) {
        case userInput.includes("?!"):
                return "Please give me some time to resolve the issue.";
        case userInput.includes("!"):
            return "Please remain calm";
        case userInput.includes("?"):
            return "Yes";
        // Add other cases as needed
        case userInput == "jamie":
            return "Can I help you?";
        default:
            return "Sorry, I don’t understand";
    }
}
